package com.fundamentosplatzi.fundamentos;

import com.fundamentosplatzi.fundamentos.component.ComponentDepndency;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundamentosApplication implements CommandLineRunner {

    private ComponentDepndency componentDependency;
    private ComponentDepndency componentTwoDependency;

    public FundamentosApplication(@Qualifier("componentImplement") ComponentDepndency componentDependency,
                                  @Qualifier("componentTwoImplement") ComponentDepndency componentTwoDependency) {
        this.componentDependency = componentDependency;
        this.componentTwoDependency = componentTwoDependency;
    }

    public static void main(String[] args) {
        SpringApplication.run(FundamentosApplication.class, args);
    }

    @Override
    public void run(String... args) {
        componentDependency.saludar();
        componentTwoDependency.saludar();
    }
}
