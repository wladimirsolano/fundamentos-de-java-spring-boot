package com.fundamentosplatzi.fundamentos.component;

import org.springframework.stereotype.Component;

@Component
public class ComponentImplement implements ComponentDepndency{
    @Override
    public void saludar() {
        System.out.println("Hola mundo desde mi componente");
    }
}
